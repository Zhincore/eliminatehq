import { readable } from "svelte/store";
import { browser } from "$app/environment";

export type Pos = [x: number, y: number];
export type PosDist = [pos: Pos, distance: number];

export const mouse = readable<PosDist>([[0, 0], 0], (set) => {
  if (!browser) return;

  const _set = (mouse: Pos) => set(processMouse(mouse));

  const mouseHandler = (ev: MouseEvent) => _set([ev.pageX, ev.pageY]);
  const touchHandler = (ev: TouchEvent) => _set([ev.touches[0].pageX, ev.touches[0].pageY]);

  document.addEventListener("mousemove", mouseHandler);
  document.addEventListener("touchmove", touchHandler);

  return () => {
    document.removeEventListener("mousemove", mouseHandler);
    document.removeEventListener("touchmove", touchHandler);
  };
});

function processMouse(mouse: Pos): [pos: Pos, distance: number] {
  const centered = [mouse[0] - window.innerWidth / 2, mouse[1] - window.innerHeight / 2];
  const distance = Math.max(0.001, Math.sqrt(centered[0] ** 2 + centered[1] ** 2));
  return [
    [centered[0] / distance, centered[1] / distance],
    distance / Math.max(window.innerWidth, window.innerHeight) / 2,
  ];
}
