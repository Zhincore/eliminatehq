import fs from "node:fs/promises";
import Path from "node:path";
import type { Root } from "hast";
import { remark } from "remark";
import remarkGfm from "remark-gfm";
import remarkParse from "remark-parse";
import remarkRehype from "remark-rehype";
import YAML from "yaml";

export const CONTENT_PATH = "content";

export type LinkPlatform =
  | "Spotify"
  | "Discord"
  | "Twitch"
  | "Facebook"
  | "Instagram"
  | "Twitter"
  | "TikTok"
  | "SoundCloud"
  | "YouTube";

export interface SiteData {
  description: string;
  email: string;
  booking: string;
  born: string;
  links: Record<"merch" | "donate" | "Songkick", string>;
  platforms: Record<LinkPlatform, [id: string, label?: string]>;
  schedule: string[];
}

export async function loadFile<T>(path: string) {
  return fs.readFile(Path.join(CONTENT_PATH, path), "utf-8").then<T>(YAML.parse);
}

const mdToHtml = remark().use(remarkParse).use(remarkGfm).use(remarkRehype).freeze();

export async function parseMarkdown(markdown: string) {
  return mdToHtml.run(mdToHtml.parse(markdown)) as Promise<Root>;
}
