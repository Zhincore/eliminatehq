import { useFrame } from "@threlte/core";
import { writable } from "svelte/store";
import { reducedMotion } from "$lib/reducedMotion";
import { colors } from "../colors";
import type { VecArray } from "../utils";

/** Core spins */
export function useSpin(reverse = false) {
  const rotation = writable(0);
  if (reducedMotion) return rotation;

  const direction = reverse ? -1 : 1;
  useFrame((_, delta) => {
    rotation.update((v) => (Math.PI * 2 + v + direction * delta) % (Math.PI * 2));
  });

  return rotation;
}

/** Fin flaps */
export function useSine3d(amplitudes: VecArray, phaseOffset = 0, fadeIn = 5) {
  const output = writable([0, 0, 0] as VecArray);
  if (reducedMotion) return output;

  let active = false;
  colors.subscribe((v) => {
    active = v.active;
  });

  let fadeInFactor = fadeIn ? 0 : 1;
  let frame = phaseOffset;
  useFrame((_, delta) => {
    if (!active) return;

    frame = (frame + delta) % (Math.PI * 2);
    if (fadeInFactor < 1) fadeInFactor = Math.min(1, fadeInFactor + (delta * 1) / fadeIn);

    output.update((v) => v.map((_, i) => amplitudes[i] && Math.sin(frame) * amplitudes[i] * fadeInFactor) as VecArray);
  });

  return output;
}
