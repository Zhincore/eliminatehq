/// <reference >

declare module "three/addons/loaders/SVGLoader.js" {
  type Loader = import("three").LoaderProto;
  export const SVGLoader: LoaderProto;
}
