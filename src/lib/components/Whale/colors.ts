import { cubicInOut } from "svelte/easing";
import { tweened } from "svelte/motion";
import { writable } from "svelte/store";
import { randomBrightness } from "$lib/randomBrightness";
import { reducedMotion } from "$lib/reducedMotion";
import type { VecArray } from "./utils";

export const colors = (() => {
  let active = !reducedMotion;
  const store = writable({ active, white: [1, 1, 1] as VecArray, emissive: [0, 0, 0] as VecArray });

  store.subscribe((v) => {
    active = v.active;
  });

  const tweenedBrightness = tweened(1, {
    duration: 500,
    easing: cubicInOut,
  });

  randomBrightness.subscribe((v) => {
    if (active) tweenedBrightness.set(v);
  });

  tweenedBrightness.subscribe((v) => {
    store.update((s) => ({
      ...s,
      white: Array(3).fill(v * 0.8) as VecArray,
    }));
  });

  return store;
})();
