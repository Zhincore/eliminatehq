import type { useLoader } from "@threlte/core";
import * as THREE from "three";
import { SVGLoader } from "three/addons/loaders/SVGLoader.js";

type Load = ReturnType<typeof useLoader<typeof SVGLoader>>["load"];

export function wrapLoad(load: Load) {
  return function (path) {
    return load(path, {
      transform(data) {
        const paths = data.paths;
        const geometries: { geometry: THREE.ShapeGeometry; emissive: boolean }[] = [];

        for (const path of paths) {
          const shapes = SVGLoader.createShapes(path);

          for (const shape of shapes) {
            const emissive = path.userData.style.fill == "lime" || path.userData.style.stroke == "lime";

            if (path.userData.style.fill != "none") {
              geometries.push({ geometry: new THREE.ShapeGeometry(shape), emissive });
            } else {
              geometries.push(
                ...path.subPaths.map((subPath: { getPoints(): unknown }) => ({
                  geometry: SVGLoader.pointsToStroke(subPath.getPoints(), path.userData.style),
                  emissive,
                }))
              );
            }
          }
        }
        return geometries;
      },
    });
  } satisfies Load;
}
