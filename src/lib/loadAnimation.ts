import type { RequestEvent } from "@sveltejs/kit";
import { writable } from "svelte/store";
import { browser } from "$app/environment";
import { page } from "$app/stores";
import { reducedMotion } from "./reducedMotion";

export const getShouldAnimateServer = (ev: RequestEvent) => ev.url.pathname == "/";

export const shouldAnimate = !reducedMotion && (!browser || window.location.pathname == "/");

export const animationsDone = writable(!shouldAnimate, (set) => {
  page.subscribe((p) => set(p.url.pathname != "/"))();
  if (!browser) return;

  const listener = (ev: KeyboardEvent) => {
    if (ev.key != "Enter") return;
    set(true);
  };

  window.addEventListener("keydown", listener);

  return () => window.removeEventListener("keydown", listener);
});

export const animationDelays = {
  logo: 0, // unused
  whale: 1,
  menu: 3.5,
  mainDone: 5, // disables skip overlay
  links: 6.5,
  done: 8, // all should be done now
};
