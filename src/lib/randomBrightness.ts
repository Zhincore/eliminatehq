import { readable } from "svelte/store";

const MIN_BRIGHTNESS = 0.3;
const MAX_BRIGHTNESS = 0.8;

export const randomBrightness = readable(0.6, (set) => {
  let timeout: NodeJS.Timeout | undefined = undefined;

  function updateBrightness() {
    set(MIN_BRIGHTNESS + Math.random() ** 0.5 * (MAX_BRIGHTNESS - MIN_BRIGHTNESS));

    timeout = setTimeout(updateBrightness, Math.random() ** 2 * 3000);
  }

  updateBrightness();
  return () => timeout && clearTimeout(timeout);
});
