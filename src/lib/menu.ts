import { faCalendar } from "@fortawesome/free-solid-svg-icons/faCalendar";
import { faEnvelope } from "@fortawesome/free-solid-svg-icons/faEnvelope";
import { faHeart } from "@fortawesome/free-solid-svg-icons/faHeart";
import { faInfo } from "@fortawesome/free-solid-svg-icons/faInfo";
import { faMapLocationDot } from "@fortawesome/free-solid-svg-icons/faMapLocationDot";
import { faMusic } from "@fortawesome/free-solid-svg-icons/faMusic";
import { faTShirt } from "@fortawesome/free-solid-svg-icons/faTShirt";
import type { ComponentProps } from "svelte";
import { quadInOut } from "svelte/easing";
import type MenuButton from "$components/MenuButton.svelte";
import type { SiteData } from "$server/content";
import { pageTransition } from "./transitions";

export type MenuItem = Omit<ComponentProps<MenuButton>, "flip" | "indexFactor" | "isReference">;

export const [sendPage, receivePage] = pageTransition({ duration: 1000, easing: quadInOut });

export const getMenu = (site?: SiteData): [MenuItem[], MenuItem[]] => [
  [
    { title: "Stream Schedule", description: "I stream on Twitch.", href: "/streams", icon: faCalendar },
    { title: "Tours", description: "See me live!", href: "/tours", icon: faMapLocationDot },
    { title: "My Music", href: "/music", icon: faMusic },
  ],
  [
    { title: "About Me", description: "Who am I?", href: "/about", icon: faInfo },
    { title: "Contact Me", href: "/contact", icon: faEnvelope },
    { title: "Merch", description: "MONSTERTRUCK OUT NOW", href: site?.links.merch ?? "#", icon: faTShirt },
    { title: "Donate", href: site?.links.donate ?? "#", icon: faHeart },
  ],
];

const mockMenu = getMenu();

export function getPage(path: string, site: SiteData) {
  for (const column of getMenu(site)) {
    for (const item of column) {
      if (item.href == path) return item;
    }
  }
}

export function getPagePosition(path: string): [side: number, indexFactor: number] | undefined {
  for (let side = 0; side < mockMenu.length; side++) {
    const column = mockMenu[side];
    for (let index = 0; index < column.length; index++) {
      if (column[index].href == path) return [side, index / (column.length - 1)];
    }
  }
}
