import { readable } from "svelte/store";

export const currentDate = readable(new Date(), (set) => {
  const timeout = setInterval(() => set(new Date()), 1000);
  return () => clearInterval(timeout);
});
