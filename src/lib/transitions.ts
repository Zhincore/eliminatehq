import { cubicOut } from "svelte/easing";
import type { EasingFunction, TransitionConfig } from "svelte/transition";

export function customfly(
  node: Element,
  { delay = 0, duration = 400, easing = cubicOut, x = 0, y = 0, opacity = 0, scale = 1 } = {}
): TransitionConfig {
  const style = getComputedStyle(node);
  const target_opacity = +style.opacity;
  const od = target_opacity * (1 - opacity);
  return {
    delay,
    duration,
    easing,
    css: (t, u) => `
			transform: translate3d(${(1 - t) * x}px, ${(1 - t) * y}px, 0) scale(${t + scale * u});
			opacity: ${target_opacity - od * u}`,
  };
}

export interface PageTransitionParams {
  delay?: number;
  duration?: number;
  easing?: EasingFunction;
  href: string;
  isMenuItem?: boolean;
  isReference?: boolean;
}

export function pageTransition({
  ...defaults
}: Omit<PageTransitionParams, "isMenuItem" | "href">): [
  (node: Element, params: PageTransitionParams) => () => TransitionConfig,
  (node: Element, params: PageTransitionParams) => () => TransitionConfig
] {
  const menuItems = new Map<string, Element>();
  let currentReference: Element | undefined = undefined;
  let currentMenuHref = "/";

  // function
  function crossfadeMenuItem(
    menuItem: Element,
    reference: Element,
    isMain: boolean,
    params: PageTransitionParams
  ): TransitionConfig {
    const { delay = 0, duration = 1000, easing = cubicOut } = Object.assign({}, defaults, params);
    const [fromNode, toNode] = isMain ? [reference, menuItem] : [menuItem, reference];

    const from = fromNode.getBoundingClientRect();
    const to = toNode.getBoundingClientRect();
    const dw = from.width - to.width;

    return {
      delay,
      duration,
      easing,
      css: (t, u) => `
        width: ${to.width + dw * u}px;
        transform-origin: center left;
        transform: scale(${isMain ? 1 - u / 2 : 1 + u * 2})
      `,
    };
  }

  function crossfadePages(
    menuItem: Element,
    reference: Element,
    isOut: boolean,
    isMain: boolean,
    params: PageTransitionParams
  ): TransitionConfig {
    const { delay = 0, duration = 1000, easing = cubicOut } = Object.assign({}, defaults, params);

    const menuRect = menuItem.getBoundingClientRect();
    const refRect = reference.getBoundingClientRect();
    const origin = isMain ? menuRect : refRect;

    const dx = isMain ? refRect.left - menuRect.left : menuRect.left - refRect.left;
    const dy = isMain ? refRect.top - menuRect.top : menuRect.top - refRect.top;

    return {
      delay,
      duration,
      easing,
      css: (t, u) => `
          opacity: ${t};
          transform-origin: ${origin.left}px ${origin.top - origin.height}px;
          transform: translate3d(${dx * u}px, ${dy * u}px, 0) scale(${isMain ? 1 + u : 1 - u});          
        `,
    };
  }

  function transition(isOut: boolean) {
    return (node: Element, params: PageTransitionParams) => {
      if (params.isMenuItem) {
        menuItems.set(params.href, node);
      } else if (params.isReference) {
        currentReference = node;
      } else if (params.href != "/") {
        currentMenuHref = params.href;
      }

      return (): TransitionConfig => {
        const menuItem = menuItems.get(currentMenuHref);
        if (!menuItem || (params.isMenuItem && params.href != currentMenuHref)) return {};

        const reference = currentReference ?? node;
        if (params.isMenuItem || params.isReference) {
          return crossfadeMenuItem(menuItem, reference, params.isMenuItem ?? false, params);
        }
        return crossfadePages(menuItem, reference, isOut, params.href == "/", params);
      };
    };
  }

  return [
    transition(true), // Out transition
    transition(false), // In transition
  ];
}
