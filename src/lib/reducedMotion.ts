import { browser } from "$app/environment";

export const reducedMotion = browser && window.matchMedia("(prefers-reduced-motion)").matches;
