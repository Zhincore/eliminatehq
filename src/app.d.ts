// See https://kit.svelte.dev/docs/types#app

import type { MenuItem } from "$lib/menu";
import type { SiteData } from "$server/content";

// for information about these interfaces
declare global {
  namespace App {
    // interface Error {}
    // interface Locals {}
    interface PageData {
      site: SiteData;
      page?: MenuItem;
    }
    // interface Platform {}
  }

  interface ObjectConstructor {
    keys<T>(obj: T): (keyof T)[];
    entries<T>(obj: T): T extends Record<infer P, infer R> ? [P, R][] : [string, unknown][];
    entries<T, P>(obj: Record<T, P>): [T, P][];
  }
}

export {};
