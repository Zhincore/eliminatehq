import type { Handle } from "@sveltejs/kit";
import { getShouldAnimateServer } from "$lib/loadAnimation";

export const handle = (async ({ event, resolve }) => {
  return resolve(event, {
    transformPageChunk: ({ html }) => html.replace("%body%", getShouldAnimateServer(event) ? "" : "skip"),
  });
}) satisfies Handle;
