import { getPage } from "$lib/menu";
import { loadFile, type SiteData } from "$server/content";
import type { LayoutServerLoad } from "./$types";

export const prerender = true;

export const load = (async (ev) => {
  const site = await loadFile<SiteData>("site.yaml");
  return {
    site,
    page: getPage(ev.url.pathname, site),
  };
}) satisfies LayoutServerLoad;
