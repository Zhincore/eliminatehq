import fs from "node:fs/promises";
import Path from "node:path";
import { CONTENT_PATH, parseMarkdown } from "$server/content";
import type { PageServerLoad } from "./$types";

export const load = (async () => {
  return {
    content: await fs.readFile(Path.join(CONTENT_PATH, "about.md"), "utf-8").then(parseMarkdown),
  };
}) satisfies PageServerLoad;
