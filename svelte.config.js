import Path from "node:path";
import adapter from "@sveltejs/adapter-static";
import { vitePreprocess } from "@sveltejs/vite-plugin-svelte";

/** @type {import('@sveltejs/kit').Config} */
const config = {
  preprocess: vitePreprocess(),

  kit: {
    adapter: adapter(),
    alias: {
      $server: Path.resolve("./src/lib/server"),
      $components: Path.resolve("./src/lib/components"),
      $lib: Path.resolve("./src/lib"),
      $lang: Path.resolve("./lang"),
    },
  },
};

export default config;
