import { sveltekit } from "@sveltejs/kit/vite";
import { defineConfig } from "vite";

export default defineConfig({
  plugins: [sveltekit()],
  build: {
    assetsInlineLimit: 1024 * 16,
  },
  ssr: {
    noExternal: ["three", "postprocessing"],
  },
});
