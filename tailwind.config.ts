import type { Config } from "tailwindcss";
import plugin from "tailwindcss/plugin";

export default {
  content: ["./src/app.html", "./src/**/*.{svelte,ts}"],
  theme: {
    extend: {
      colors: {
        accent: "#70ff40",
        background: "#111",
        foreground: "#ccc",
      },
      transitionDuration: {
        DEFAULT: "300ms",
      },
      fontFamily: {
        title: "HemiHead",
      },
      dropShadow: {
        glow: "0 0px 4px color-mix(in srgb, currentColor 50%, transparent)",
        "glow-md": "0 0px 8px color-mix(in srgb, currentColor 75%, transparent)",
        "glow-lg": "0 0px 16px color-mix(in srgb, currentColor 75%, transparent)",
      },
    },
  },
  plugins: [
    plugin(function ({ addVariant }) {
      addVariant("hover-focus", ["&:hover", "&:focus", "&:focus-within"]);
      addVariant("group-hover-focus", [".group:hover &", ".group:focus &", ".group:focus-within &"]);
      addVariant("peer-hover-focus", [".peer:hover ~ &", ".peer:focus ~ &", ".peer:focus-within ~ &"]);
      addVariant("noscript", ["body.noscript &"]);
      addVariant("script", ["body:not(.noscript) &"]);
    }),
  ],
} satisfies Config;
