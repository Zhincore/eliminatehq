# use the official Bun image
# see all versions at https://hub.docker.com/r/oven/bun/tags
FROM oven/bun:1-alpine AS base
WORKDIR /usr/src/app

# install dependencies into temp directory
# this will cache them and speed up future builds
FROM base AS install
WORKDIR /tmp/dev
COPY package.json bun.lockb ./
RUN bun install --frozen-lockfile

# copy node_modules from temp directory
# then copy all (non-ignored) project files into the image
FROM base AS prerelease

WORKDIR /usr/src/app
COPY --from=install /tmp/dev/node_modules node_modules
COPY . .

# Build project 
RUN bun --bun run vite build

# copy production dependencies and source code into final image
FROM base AS release
WORKDIR /usr/src/app

COPY --from=prerelease /usr/src/app/build ./build
COPY --from=prerelease /usr/src/app/content ./content
COPY --from=prerelease /usr/src/app/node_modules ./node_modules

ENV PROTOCOL_HEADER="x-forwarded-proto"
ENV HOST_HEADER="x-forwarded-host"

# run the app
EXPOSE 3000/tcp
ENTRYPOINT [ "bun", "run", "./build/index.js" ]
